import React from "react";
import TodayForcast from "./todayForcast";
import WeeklyForcast from "./weeklyForcast";
const Forcast = () => {

    return (
        <>
            <TodayForcast/>
            <WeeklyForcast/>
        </>
    )

}
export default Forcast;