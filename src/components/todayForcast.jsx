import React from "react";
import {useSelector} from "react-redux";
import "react-toastify/dist/ReactToastify.css";
const TodayForcast = () => {

    const data  = useSelector((state) => state.weather);
    const weatherDetails = !!data.weather ? data.weather : '' ;
    return (
        <div className="card" style={{color: '#4B515D', borderRadius: '35px'}}>
            <div className="card-body p-4">
                {weatherDetails.cod != 404 && !!weatherDetails ? (
                    <>
                        <div className="d-flex">
                            <h6 className="flex-grow-1">Today's Forecast for {!!weatherDetails && weatherDetails.city}</h6>
                        </div>
                        <div className="d-flex flex-column text-center mt-5 mb-4">
                            <h6 className="display-4 mb-0 font-weight-bold" style={{color: '#1C2331'}}> {!!weatherDetails && weatherDetails?.main?.temp}{weatherDetails?.unit == 'metric' ? '°C' : '℉' }  </h6>
                            <span className="small" style={{color: '#868B94'}}>{!!weatherDetails?.weather[0] && weatherDetails?.weather[0]?.description}</span>
                        </div>
                        <div className="d-flex align-items-center">
                            <div className="flex-grow-1" style={{fontSize: '1rem'}}>
                                <div><i className="fas fa-wind fa-fw" style={{color: '#868B94'}} /> <span className="ms-1"> <b> Weather Condition:</b> {!!weatherDetails?.weather[0] && weatherDetails?.weather[0]?.main}
                        </span></div>
                                <div><i className="fas fa-tint fa-fw" style={{color: '#868B94'}} /> <span className="ms-1"> <b> Highest Temp: </b> {!!weatherDetails && weatherDetails?.main?.temp_max}{weatherDetails?.unit == 'metric' ? '°C' : '℉' } </span>
                                </div>
                                <div><i className="fas fa-sun fa-fw" style={{color: '#868B94'}} /> <span className="ms-1"> <b> Lowest Temp:</b> {!!weatherDetails && weatherDetails?.main?.temp_min}{weatherDetails?.unit == 'metric' ? '°C' : '℉' } </span>
                                </div>
                            </div>
                            <div>
                                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-weather/ilu1.webp" width="100px" />
                            </div>
                        </div>
                    </>

                ) : (
                    <div className="d-flex">
                        <h6 className="flex-grow-1">No Record Found</h6>
                    </div>
                )}

            </div>
        </div>
    )
}

export default TodayForcast;


