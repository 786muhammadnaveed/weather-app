import React, {useState} from "react";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button"

const Search = ({onHandleInputChange}) => {

    const [inputValue, setInputValue] = useState('');

    const onHandleInputClick = () => {
        onHandleInputChange(inputValue);
    }

   return (
       <div className="App pt-5 mt-3 mb-4">
           <div className="container h-100">
               <div className="row h-100 justify-content-center align-items-center"></div>
               <InputGroup className="col-6">
                   <FormControl
                       placeholder="Enter Name To Search The Weather"
                       aria-label="Search"
                       aria-describedby="basic-addon2"
                       onChange = {(e) => setInputValue(e.target.value)}
                   />
                   <Button variant="outline-primary bg-primary text-white" id="button-addon2" type="button" onClick={onHandleInputClick}>
                       Search
                   </Button>
               </InputGroup>
           </div>
       </div>
   )
}

export default Search;