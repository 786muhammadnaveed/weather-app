import React from "react";
import Slider from "react-slick";
import {useSelector} from "react-redux";

const WeeklyForcast = () => {

    const data  = useSelector((state) => state);
    const weeklyWeather = !!data.weather.weeklyWeather && data.weather.weeklyWeather.length > 0 ? data.weather.weeklyWeather : '' ;

    var settings = {
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
    };

    return (
        <>
            <h4 className="mb-4 mt-4 pt-2">Weekly Forecast for {weeklyWeather[0]?.city}</h4>
            <Slider {...settings}>
                {weeklyWeather && weeklyWeather?.map((data, index) => (
                    <div className="card" key={index}>

                        <div className="card-body p-4">
                            <div className="d-flex">
                                {!!weeklyWeather.days[index] && weeklyWeather.days[index]}
                            </div>
                            <div className="d-flex flex-column text-center mt-5 mb-4">
                                <h5 className="mb-0 font-weight-bold" style={{color: '#1C2331'}}> {data?.main?.temp} {weeklyWeather[0]?.unit == 'metric' ? '°C' : '℉' } </h5>
                                <span className="small" style={{color: '#868B94'}}>{data?.weather[0]?.description}</span>
                            </div>
                            <div className="d-flex align-items-center">
                                <div className="flex-grow-2" style={{fontSize: '1rem'}}>
                                    <div><i className="fas fa-wind fa-fw" style={{color: '#868B94'}} />
                                        <span className="ms-1"> <b> Condition:</b>
                                            {data?.weather[0]?.main}
                                        </span>
                                    </div>
                                    <div><i className="fas fa-tint fa-fw" style={{color: '#868B94'}} /> <span className="ms-1"> <b> H Temp: </b> {data?.main?.temp_max}{weeklyWeather[0]?.unit == 'metric' ? '°C' : '℉' }  </span>
                                    </div>
                                    <div><i className="fas fa-sun fa-fw" style={{color: '#868B94'}} /> <span className="ms-1"> <b> L Temp:</b>{data?.main?.temp_min}{weeklyWeather[0]?.unit == 'metric' ? '°C' : '℉' } </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </Slider>
        </>
    );
}

export default WeeklyForcast;