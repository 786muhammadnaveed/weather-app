import React from "react";
import {Container,Navbar} from "react-bootstrap";

const Header = () => {
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#home">
                    <img
                        alt=""
                        src="/emumba.jpeg"
                        width="30"
                        height="30"
                        className="d-inline-block align-top"
                    />{' '}
                    The Weather App
                </Navbar.Brand>
            </Container>
        </Navbar>
    )
}

export default Header;