import React from "react";
import {Form} from "react-bootstrap";

const TempConverter = ({onHandleUnitChange}) => {

    return (
        <>
            <h4>Temperature Convertor</h4>
            <Form.Group controlId="formBasicSelect">
                <Form.Label>Select Unit</Form.Label>
                <Form.Control
                    as="select"
                    onChange={(e) => onHandleUnitChange(e.target.value)}
                >
                    <option value="metric" selected>Celsius</option>
                    <option value="imperial">Fahrenheit</option>
                </Form.Control>
            </Form.Group>
        </>
    )
}

export default TempConverter;