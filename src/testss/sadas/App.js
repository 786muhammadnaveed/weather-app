import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';
import WeatherPage from "./pages/WeatherPage";
import {StoreProvider} from "easy-peasy";
import store from "./redux";

function App() {
  return (
      <StoreProvider store={store}>
        <WeatherPage/>
      </StoreProvider>
  )
}

export default App;
