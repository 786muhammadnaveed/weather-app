import React, {useState,useEffect} from "react";
import Navbar from "../components/navbar";
import Search from "../components/search";
import Forcast from "../components/forcast";
import {Container} from "react-bootstrap";
import "react-toastify/dist/ReactToastify.css";
import { useSelector, useDispatch } from "react-redux";
import {getWeatherAsync, getWeeklyWeatherAsync} from "../redux/weatherReducer";
import WeeklyForcast from "../components/weeklyForcast";
import TempConverter from "../components/tempConverter";

const WeatherPage = () => {

    const dispatch = useDispatch();
    const [city, setCity] = useState('Islamabad')
    const [unit, setUnit] = useState('metric')
    const error  = useSelector((state) => state.weather.error);

    useEffect(() => {
        dispatch(getWeeklyWeatherAsync(city,unit));
    },[city,unit])

    const onHandleInputChange = value => {
        setCity(value);
    }

    const onHandleUnitChange = unit => {
        setUnit(unit);
    }

    return (
        <>
            <Navbar/>
            <Search onHandleInputChange={onHandleInputChange} />
            <Container>
                <div className="row">

                    {!!error && error == "Request failed with status code 404" ? (
                        <h1 className="text-danger mt-2 text-center">No Record Found </h1>
                    ): (
                        <>
                            <div className="col-md-6">
                                <Forcast/>
                            </div>

                            <div className="col-md-6">
                                <TempConverter onHandleUnitChange={onHandleUnitChange}/>
                            </div>
                        </>
                    )}
                </div>

            </Container>
        </>
    )
}

export default WeatherPage;