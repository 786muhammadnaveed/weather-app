import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import './App.css';
import WeatherPage from "./pages/WeatherPage";

function App() {
  return (
      <>
        <WeatherPage/>
      </>
  )
}

export default App;
