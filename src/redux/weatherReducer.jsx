import { createSlice } from "@reduxjs/toolkit";
import {REACT_APP_API_KEY, REACT_APP_API_URL} from "../components/ApiConstants";
import axios from "axios";

export const weatherSlice = createSlice({
    name: "weather",
    initialState: {
        weather: '',
        weeklyWeather :'',
        error: '',
    },
    reducers: {
        getWeather: (state, action) => {
            state.weather = action.payload;
        },
        getWeeklyWeather: (state, action) => {
            state.weeklyWeather = action.payload;
        },
        getError: (state, action) => {
            state.error = action.payload
        }
    }
});


export const getWeeklyWeatherAsync = (city = "Islamabad", unit = "metric") => async (dispatch) => {
    try {

        const url = `${REACT_APP_API_URL}/forecast?q=${city}&zip=90210&appid=${REACT_APP_API_KEY}&units=${unit}`
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        axios.get(url)
            .then(function (res) {
                let dailyData = res?.data?.list?.slice(0, 7);
                let nexDay = new Date(dailyData[0]?.dt * 1000).toLocaleDateString("en", {
                    weekday: "long",
                });

                let index = days.indexOf(nexDay);
                const citrus = days.slice(index);
                const filter = days.filter(function(obj) { return citrus.indexOf(obj) == -1; });

                dailyData['days'] = [...citrus,...filter];
                dailyData[0]['city'] = res?.data?.city?.name;
                dailyData[0]['unit'] = unit;
                dispatch(getError(''));
                dispatch(getWeeklyWeather(dailyData));
                dispatch(getWeather(dailyData[0]));
            })
            .catch(function (error) {
                dispatch(getError(error.message));
            })

    } catch (err) {
        throw new Error(err);
    }
};

// Action creators are generated for each case reducer function
export const { getWeather,getWeeklyWeather,getError } = weatherSlice.actions;

export default weatherSlice.reducer;
